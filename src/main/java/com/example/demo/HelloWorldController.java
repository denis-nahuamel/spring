package com.example.demo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class HelloWorldController {
	//get uri /hello-world
	@GetMapping(path= "/hello-world")
	public String helloWorld() {
		return "hola mundo";
	}
	
	//get uri /hello-world-bean
		@GetMapping(path= "/hello-world-bean")
		public HelloWorldBean helloWorldBean() {
			return new HelloWorldBean("Hola desde gitpod");
		}

}
